# Neuro Science Project

## Requirement
 - [Docker](https://www.docker.com/get-started)
 - [Lando installed](https://lando.dev/download/)

## Project Setup

Follow these steps to set up the project:

### 1. Start lando
Initialize and start the lando environment.
Inside project root, run:
`lando start`
### 2. Import the Database
If you have a database dump, you can import it using lando:

```lando db-import ./backup/drupal10.sql.gz```
### 3. Install Drupal
Run: `lando composer install`
### 4. Update local settings for Drupal

1. Run: `cp web/sites/default/example.services.local.yml web/sites/default/services.local.yml`

2. Apply the same with settings.local.php
`cp web/sites/default/example.settings.local.php web/sites/default/settings.local.php`

> **For Drupal Admin**
After all the steps above, the site should be available at `https://neuro-science.lndo.site/`

#### If you catch mysqld service took a lot of memory, try to add mariadb service instead of mysql - the default one.
```
services:
  ...
  database:
    type: mariadb
  ...
```
#### If you got the error message of `drupal10 is not a supported recipe type`
Try `lando plugin-add @lando/drupal` then `lando start`. If it still not work, try to downgrade lando back to **3.20.8**

### 5. To run storybook as Front End development
In the project root, run
```
lando npm install
lando npm run build-storybook
lando npm run storybook
```
The storybook is available at `http://localhost:6006/`

Components List are located at: web/themes/custom/rmhnf/components

>Everytime create a new component, or marking component update, to reload storybook rendering, use:
> - `lando drush cr` // To clear cache
>
>   And
> - `lando npm run storybook` // To run the storybook
