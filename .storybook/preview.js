/** @type { import('@storybook/server').Preview } */
const preview = {
  parameters: {
    actions: { argTypesRegex: "^on[A-Z].*" },
    controls: {
      matchers: {
        color: /(background|color)$/i,
        date: /Date$/,
      },
    },
    server: {
      // Replace this with your Drupal site URL, or an environment variable.
      url: 'https://neuro-science.lndo.site/',
    },
  },
  globals: {
    drupalTheme: 'rmhnf',
    supportedDrupalThemes: {
      rmhnf: {title: 'rmhnf Theme'},
      bartik: {title: 'Bartik'},
      claro: {title: 'Claro'},
      seven: {title: 'Seven'},
    },
  },
};
export default preview;
